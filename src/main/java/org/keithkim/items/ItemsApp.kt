package org.keithkim.items;

import io.javalin.Javalin
import io.javalin.core.util.JettyServerUtil
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.keithkim.items.db.Database
import org.keithkim.items.db.ItemDao
import org.keithkim.items.db.TypeDao
import org.slf4j.LoggerFactory
import java.util.function.Supplier

class ItemsApp {
}

fun main(args: Array<String>) {
    val port = Integer.parseInt(System.getProperty("PORT", "8080"))
    val jettyServer = JettyServerUtil.defaultServer()
    jettyServer.apply {
        connectors = arrayOf(ServerConnector(jettyServer).apply {
            this.host = System.getProperty("HOST", "0.0.0.0")
            this.port = port
        })
    }
    val app = Javalin.create()
            .disableStartupBanner()
            .enableCorsForOrigin("*")
            .port(port)
            .server { jettyServer }
            .start()

    val db = Database.fromEnvVar()

    val log = LoggerFactory.getLogger(ItemsApp::class.java)
    ItemsApp::class.java.getResourceAsStream("/banner.txt").use {
        val banner = it.bufferedReader().use { it.readText() }
        log.info(banner)
    }

    app.get("/") { ctx ->
        ctx.result("Hello World")
    }

    app.get("/types") { ctx ->
        val parent = ctx.queryParams("parent").firstOrNull()
        val types = db.withTypeDao { dao: TypeDao ->
            if (parent == null) {
                dao.findAll()
            } else {
                dao.findByParent(parent)
            }
        }
        ctx.json(types)
    }

    app.get("/types/:type") { ctx ->
        val typeParam = ctx.pathParam("type")
        val type = db.withTypeDao { dao: TypeDao ->
            dao.findById(typeParam)
        }
        ctx.json(type)
    }

    app.get("/items") { ctx ->
        val items = db.withItemDao { dao: ItemDao ->
            dao.findAll()
        }
        ctx.json(items)
    }

    app.get("/types/:type/items") { ctx ->
        val type = ctx.pathParam("type")
        val items = db.withItemDao { dao: ItemDao ->
            dao.findByType(type)
        }
        ctx.json(items)
    }
}
