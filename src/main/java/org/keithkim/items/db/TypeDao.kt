package org.keithkim.items.db

import org.jdbi.v3.sqlobject.config.RegisterRowMapper
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate

@RegisterRowMapper(TypeMapper::class)
interface TypeDao {
    @SqlQuery("SELECT * FROM type ORDER BY parent, name")
    fun findAll(): List<Type>

    @SqlQuery("SELECT * FROM type WHERE name = :name")
    fun findById(name: String): Type

    @SqlQuery("SELECT * FROM type WHERE parent = :parent ORDER BY name")
    fun findByParent(parent: String): List<Type>

    @SqlQuery("INSERT INTO type (name, parent) VALUES (:name, :parent)")
    fun createType(name: String, parent: String?): Int

    @SqlUpdate("DELETE FROM type WHERE name = :name")
    fun deleteById(name: String)
}
