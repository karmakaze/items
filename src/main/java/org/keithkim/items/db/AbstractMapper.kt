package org.keithkim.items.db

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

abstract class AbstractMapper {
    val objectMapper = jacksonObjectMapper()
}
