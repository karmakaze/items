package org.keithkim.items.db

import com.zaxxer.hikari.HikariDataSource
import org.jdbi.v3.core.Handle
import org.jdbi.v3.core.Jdbi
import org.jdbi.v3.core.extension.ExtensionCallback
import org.jdbi.v3.core.kotlin.KotlinPlugin

import javax.sql.DataSource
import java.net.URI
import java.net.URISyntaxException
import java.sql.SQLException
import java.util.Optional
import org.jdbi.v3.sqlobject.SqlObjectPlugin
import org.jdbi.v3.sqlobject.kotlin.KotlinSqlObjectPlugin

class Database constructor(url: String, username: String, password: String) {
    public val jdbi: Jdbi

    init {
        val ds = HikariDataSource()
        ds.jdbcUrl = url
        ds.username = username
        ds.password = password

        val jdbi = Jdbi.create(ds)
        jdbi.installPlugin(SqlObjectPlugin())
        jdbi.installPlugin(KotlinPlugin())
        jdbi.installPlugin(KotlinSqlObjectPlugin())
        this.jdbi = jdbi
    }

    interface DaoCallback<R, E> {
        @Throws(Exception::class)
        fun withDao(dao: E): R
    }

    fun <R> withTypeDao(f: (TypeDao)->R): R {
        return jdbi.withExtension<R, TypeDao, Exception>(TypeDao::class.java) { dao ->
            f.invoke(dao)
        }
    }
    fun <R> withItemDao(f: (ItemDao)->R): R {
        return jdbi.withExtension<R, ItemDao, Exception>(ItemDao::class.java) { dao ->
            f.invoke(dao)
        }
    }

    companion object {

        @Throws(URISyntaxException::class)
        fun fromEnvVar(): Database {
            val dbUrl = Optional.ofNullable(System.getenv("DATABASE_URL"))
                    .orElse("mysql://items:items@127.0.0.1:3306/items?useSSL=false&autoReconnect=true")
            val dbUri = URI(dbUrl)

            val username = dbUri.userInfo.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
            val password = dbUri.userInfo.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
            val jdbcUrl = "jdbc:mysql://" + dbUri.host + ':'.toString() + dbUri.port + dbUri.path

            return Database(jdbcUrl, username, password)
        }

        fun forIntegrationTesting(): Database {
            val dbHost = Optional.ofNullable(System.getenv("MYSQL_HOST")).orElse("54.173.5.37")

            return Database(
                    "jdbc:mysql://$dbHost:5432/items_test",
                    "mysql",
                    "100Engaged"
            )
        }

        // based on https://devcenter.heroku.com/articles/connecting-to-relational-databases-on-heroku-with-java#using-the-database_url-in-plain-jdbc
        @Throws(URISyntaxException::class, SQLException::class)
        private fun getDataSource(): DataSource {
            val dbUri = URI(System.getenv("DATABASE_URL"))

            val username = dbUri.userInfo.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
            val password = dbUri.userInfo.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]
            val dbUrl = "jdbc:mysql://" + dbUri.host + ':'.toString() + dbUri.port + dbUri.path

            val ds = HikariDataSource()
            ds.jdbcUrl = dbUrl
            ds.username = username
            ds.password = password
            return ds
        }
    }
}
