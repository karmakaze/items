package org.keithkim.items.db

import com.fasterxml.jackson.module.kotlin.readValue
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.sql.SQLException

class ItemMapper : AbstractMapper(), RowMapper<Item> {

    @Throws(SQLException::class)
    override
    fun map(rs: ResultSet, ctx: StatementContext): Item {
        val attrStr = rs.getString("attributes") ?: ""
        val attributes: Map<String, Any> = if (attrStr == "") {
            emptyMap()
        } else {
            objectMapper.readValue(attrStr)
        }
        return Item(
                id = rs.getLong("id"),
                type = rs.getString("type"),
                name = rs.getString("name"),
                attributes = attributes
        )
    }
}
