package org.keithkim.items.db

import com.fasterxml.jackson.module.kotlin.readValue
import org.jdbi.v3.core.mapper.RowMapper
import org.jdbi.v3.core.statement.StatementContext
import java.sql.ResultSet
import java.sql.SQLException

class TypeMapper : AbstractMapper(), RowMapper<Type> {

    @Throws(SQLException::class)
    override
    fun map(rs: ResultSet, ctx: StatementContext): Type {
        val attrStr = rs.getString("attributes") ?: ""
        val attributes: Map<String, Any> = if (attrStr == "") {
            emptyMap()
        } else {
            objectMapper.readValue(attrStr)
        }
        return Type(
                name = rs.getString("name"),
                parent = rs.getString("parent") ?: "",
                attributes = attributes
        )
    }
}
