package org.keithkim.items.db;

import org.jdbi.v3.sqlobject.config.RegisterRowMapper
import org.jdbi.v3.sqlobject.statement.SqlQuery
import org.jdbi.v3.sqlobject.statement.SqlUpdate

@RegisterRowMapper(ItemMapper::class)
interface ItemDao {

    @SqlQuery("SELECT * FROM item")
    fun findAll(): List<Item>

    @SqlQuery("SELECT * FROM item WHERE type = :type AND name = :name")
    fun findById(type: String, name: String): Item

    @SqlQuery("SELECT * FROM item WHERE type = :type ORDER BY name")
    fun findByType(type: String): List<Item>

    @SqlQuery("INSERT INTO item (type, name) VALUES (:item.type, :item.name)")
    fun createItem(item: Item): Int

    @SqlQuery("UPDATE item" +
            " SET attributes = :item.attributes" +
            " WHERE type = :item.name AND name = :item.name" +
            " RETURNING *")
    fun updateItem(item: Item): Item

    @SqlUpdate("DELETE FROM item WHERE type = :item.type AND name = :item.name")
    fun deleteById(id: Int): Unit

}
