package org.keithkim.items.db

data class Item (
    val id: Long,
    val type: String,
    val name: String?,
    val attributes: Map<String, Any>
)
