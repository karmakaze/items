package org.keithkim.items.db

data class Type (
    val name: String,
    val parent: String?,
    val attributes: Map<String, Any>
)
